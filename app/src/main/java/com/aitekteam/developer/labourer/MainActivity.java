package com.aitekteam.developer.labourer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.aitekteam.developer.labourer.adapters.MainViewPagerAdapter;
import com.aitekteam.developer.labourer.fragments.AdminProfileFragment;
import com.aitekteam.developer.labourer.fragments.AdminWorkFragment;
import com.aitekteam.developer.labourer.fragments.FindWorkersFragment;
import com.aitekteam.developer.labourer.fragments.GuestProfileFragment;
import com.aitekteam.developer.labourer.fragments.GuestWorkFragment;
import com.aitekteam.developer.labourer.fragments.JobSearchFragment;
import com.aitekteam.developer.labourer.fragments.NotificationFragment;
import com.aitekteam.developer.labourer.models.M_User;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private LinearLayout container;
    private ProgressBar loader;
    private BottomNavigationView mainMenu;
    private ViewPager mainPager;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private MainViewPagerAdapter adapter;
    private ArrayList<Fragment> items;
    private ArrayList<Integer> titles;
    private MenuItem prevMenuItem;
    private ValueEventListener getProfile = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.exists()) {
                M_User profile = dataSnapshot.getValue(M_User.class);
                if (profile != null) {
                    setVisibility(1);
                    if (profile.getUser_type() == 0)
                        setUpGuestContent();
                    else
                        setUpAdminContent();

                    mainPager.setAdapter(adapter);
                    mainMenu.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.item_work_admin:
                                    mainPager.setCurrentItem(0);
                                    break;
                                case R.id.item_work_guest:
                                    mainPager.setCurrentItem(0);
                                    break;
                                case R.id.item_find_job:
                                    mainPager.setCurrentItem(1);
                                    break;
                                case R.id.item_find_workers:
                                    mainPager.setCurrentItem(1);
                                    break;
                                case R.id.item_notification:
                                    mainPager.setCurrentItem(2);
                                    break;
                                case R.id.item_profile:
                                    mainPager.setCurrentItem(3);
                                    break;
                            }
                            return false;
                        }
                    });
                    mainPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            if (prevMenuItem != null)
                                prevMenuItem.setChecked(false);
                            else
                                mainMenu.getMenu().getItem(0).setChecked(false);

                            mainMenu.getMenu().getItem(position).setChecked(true);
                            prevMenuItem = mainMenu.getMenu().getItem(position);
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.container = findViewById(R.id.main_container);
        this.loader = findViewById(R.id.loader);
        this.mainMenu = findViewById(R.id.main_menu);
        this.mainPager = findViewById(R.id.main_pager);

        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = this.mAuth.getCurrentUser();
        if (currentUser == null) gotoLogin();

        this.items = new ArrayList<>();
        this.titles = new ArrayList<>();

        this.setUp();
    }

    private void setUp() {
        setVisibility(0);
        if (this.mAuth.getUid() != null)
            this.mDatabase.child("m_users").child(this.mAuth.getUid()).addListenerForSingleValueEvent(getProfile);
    }

    private void setUpAdminContent() {
        this.adapter = new MainViewPagerAdapter(getSupportFragmentManager(), this, new AdminWorkFragment(), R.string.work_name);

        this.items.add(new AdminWorkFragment());
        this.items.add(new FindWorkersFragment());
        this.items.add(new NotificationFragment());
        this.items.add(new AdminProfileFragment());

        this.titles.add(R.string.work_name);
        this.titles.add(R.string.admin_find_workers_name);
        this.titles.add(R.string.notification_name);
        this.titles.add(R.string.profile_name);

        this.adapter.setItems(this.items);
        this.adapter.setTitles(this.titles);
    }

    private void setUpGuestContent() {
        this.adapter = new MainViewPagerAdapter(getSupportFragmentManager(), this, new AdminWorkFragment(), R.string.work_name);

        this.items.add(new GuestWorkFragment());
        this.items.add(new JobSearchFragment());
        this.items.add(new NotificationFragment());
        this.items.add(new GuestProfileFragment());

        this.titles.add(R.string.work_name);
        this.titles.add(R.string.guest_find_job_name);
        this.titles.add(R.string.notification_name);
        this.titles.add(R.string.profile_name);

        this.adapter.setItems(this.items);
        this.adapter.setTitles(this.titles);
    }

    private void gotoLogin() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void setVisibility(int action) {
        if (action == 0) {
            loader.setVisibility(View.VISIBLE);
            container.setVisibility(View.GONE);
        }
        else {
            loader.setVisibility(View.GONE);
            container.setVisibility(View.VISIBLE);
        }
    }
}
