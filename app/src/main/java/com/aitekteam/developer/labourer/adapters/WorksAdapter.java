package com.aitekteam.developer.labourer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aitekteam.developer.labourer.R;
import com.aitekteam.developer.labourer.interfaces.CustomClickHandler;
import com.aitekteam.developer.labourer.models.K_Guest;
import com.aitekteam.developer.labourer.models.M_Work;

import java.util.ArrayList;

public class WorksAdapter extends RecyclerView.Adapter<WorksAdapter.ViewHolder> {

    public static int FOR_GUEST = 0, FOR_ADMIN = 1;

    private ArrayList<M_Work> items;
    private CustomClickHandler handler;
    private Context context;
    private int for_owner;
    private ArrayList<K_Guest> guest;

    public WorksAdapter(CustomClickHandler handler, int for_owner) {
        this.handler = handler;
        this.for_owner = for_owner;
        this.items = new ArrayList<>();
    }

    public void setItems(ArrayList<M_Work> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void setGuest(ArrayList<K_Guest> guest) {
        this.guest = guest;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        M_Work item = this.items.get(position);
        holder.location.setText(item.getLocation_name());
        holder.status.setText(this.getStatus(item.getStatus(), position));
        holder.title.setText(item.getTitle());
        holder.type.setImageResource(this.getCategory(item.getType_of_work().getCategory_id()));
    }

    private int getCategory(int category) {
        switch (category) {
            case 0: return R.mipmap.ic_painting;
            case 1: return R.mipmap.ic_bricklayer;
            case 2: return R.mipmap.ic_carpenter;
            case 3: return R.mipmap.ic_blacksmith;
            default: return R.mipmap.ic_digger;
        }
    }

    private String getStatus(long status, int position) {
        if (this.guest != null && this.for_owner == FOR_GUEST) {
            switch (this.guest.get(position).getStatus()) {
                case 0:
                    return this.context.getString(R.string.status_waiting);
                case 1:
                    return this.context.getString(R.string.status_accepted);
                case 2:
                    return this.context.getString(R.string.status_rejected);
                case 3:
                    return this.context.getString(R.string.status_ongoing);
                default:
                    return this.context.getString(R.string.status_done);
            }
        }
        else {
            if (status == 0) return this.context.getString(R.string.status_waiting_worker);
            else if (status == 1) return this.context.getString(R.string.status_ongoing);
            else return this.context.getString(R.string.status_done);
        }
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView type;
        TextView title, location, status;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            type = itemView.findViewById(R.id.item_type);
            title = itemView.findViewById(R.id.item_title);
            location = itemView.findViewById(R.id.item_location);
            status = itemView.findViewById(R.id.item_status);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    handler.onItemClick(items.get(getAdapterPosition()), getAdapterPosition(), for_owner);
                }
            });
        }
    }
}
