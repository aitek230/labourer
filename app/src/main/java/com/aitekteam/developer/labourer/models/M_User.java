package com.aitekteam.developer.labourer.models;

import java.io.Serializable;

public class M_User implements Serializable {
    private String
            id,
            user_id,
            email,
            full_name,
            password,
            address,
            phone_number,
            face;
    private int user_type;

    public M_User(String id, String user_id, String email, String full_name, String password, String address, String phone_number, String face, int user_type) {
        this.id = id;
        this.user_id = user_id;
        this.email = email;
        this.full_name = full_name;
        this.password = password;
        this.address = address;
        this.phone_number = phone_number;
        this.face = face;
        this.user_type = user_type;
    }

    public M_User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public int getUser_type() {
        return user_type;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }
}
