package com.aitekteam.developer.labourer.models;

import java.io.Serializable;
import java.util.ArrayList;

public class M_Work implements Serializable {
    private ArrayList<String> previews;
    private ArrayList<String> benefits;
    private String id, admin_id, title, description, location_name;
    private long prices, priceOfDay;
    private int count_of_day, count_of_worker, status;
    private M_Expertise_Category type_of_work;

    public M_Work() {
    }

    public M_Work(ArrayList<String> previews, ArrayList<String> benefits, String id, String admin_id, String title, String description, String location_name, long prices, long priceOfDay, int count_of_day, int count_of_worker, int status, M_Expertise_Category type_of_work) {
        this.previews = previews;
        this.benefits = benefits;
        this.id = id;
        this.admin_id = admin_id;
        this.title = title;
        this.description = description;
        this.location_name = location_name;
        this.prices = prices;
        this.priceOfDay = priceOfDay;
        this.count_of_day = count_of_day;
        this.count_of_worker = count_of_worker;
        this.status = status;
        this.type_of_work = type_of_work;
    }

    public ArrayList<String> getPreviews() {
        return previews;
    }

    public void setPreviews(ArrayList<String> previews) {
        this.previews = previews;
    }

    public ArrayList<String> getBenefits() {
        return benefits;
    }

    public void setBenefits(ArrayList<String> benefits) {
        this.benefits = benefits;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public long getPrices() {
        return prices;
    }

    public void setPrices(long prices) {
        this.prices = prices;
    }

    public long getPriceOfDay() {
        return priceOfDay;
    }

    public void setPriceOfDay(long priceOfDay) {
        this.priceOfDay = priceOfDay;
    }

    public int getCount_of_day() {
        return count_of_day;
    }

    public void setCount_of_day(int count_of_day) {
        this.count_of_day = count_of_day;
    }

    public int getCount_of_worker() {
        return count_of_worker;
    }

    public void setCount_of_worker(int count_of_worker) {
        this.count_of_worker = count_of_worker;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public M_Expertise_Category getType_of_work() {
        return type_of_work;
    }

    public void setType_of_work(M_Expertise_Category type_of_work) {
        this.type_of_work = type_of_work;
    }
}