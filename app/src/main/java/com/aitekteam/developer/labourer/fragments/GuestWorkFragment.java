package com.aitekteam.developer.labourer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aitekteam.developer.labourer.LoginActivity;
import com.aitekteam.developer.labourer.R;
import com.aitekteam.developer.labourer.adapters.WorksAdapter;
import com.aitekteam.developer.labourer.interfaces.CustomClickHandler;
import com.aitekteam.developer.labourer.models.K_Guest;
import com.aitekteam.developer.labourer.models.M_Work;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class GuestWorkFragment extends Fragment {

    private WorksAdapter adapter;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private ChildEventListener getWorks = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            setUpGetWorks(dataSnapshot);
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private void setUpGetWorks(DataSnapshot dataSnapshot) {
        if (dataSnapshot.getChildrenCount() > 0) {
            ArrayList<M_Work> items = new ArrayList<>();
            ArrayList<K_Guest> k_guest = new ArrayList<>();
            for (DataSnapshot item : dataSnapshot.getChildren()) {
                items.add(item.getValue(M_Work.class));
                if (item.hasChild("workers")) {
                    for (DataSnapshot item_worker: item.child("workers").getChildren()) {
                        k_guest.add(item_worker.getValue(K_Guest.class));
                    }
                }
            }

            if (items.size() > 0) this.adapter.setItems(items);
            if (k_guest.size() > 0) this.adapter.setGuest(k_guest);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_guest_works, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.mDatabase = FirebaseDatabase.getInstance().getReference();
        this.mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = this.mAuth.getCurrentUser();
        if (currentUser == null) gotoLogin();

        RecyclerView worksList = view.findViewById(R.id.works_list);
        this.adapter = new WorksAdapter(new CustomClickHandler() {
            @Override
            public void onItemClick(Object item, int position, int action) {

            }
        }, WorksAdapter.FOR_GUEST);
        worksList.setLayoutManager(new LinearLayoutManager(getContext()));
        worksList.setAdapter(this.adapter);

        if (this.mAuth.getUid() != null)
            this.mDatabase.child("m_users").child(this.mAuth.getUid())
                    .child("works").addChildEventListener(getWorks);
    }

    private void gotoLogin() {
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        if (getActivity() != null)
            getActivity().finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (this.mAuth.getUid() != null)
            this.mDatabase.child("m_users").child(this.mAuth.getUid())
                    .child("works").removeEventListener(getWorks);
    }
}
