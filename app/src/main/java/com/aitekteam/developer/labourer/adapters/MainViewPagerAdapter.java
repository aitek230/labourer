package com.aitekteam.developer.labourer.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class MainViewPagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    private ArrayList<Fragment> items;
    private ArrayList<Integer> titles;
    private Fragment fragment_default;
    private int default_title;

    public MainViewPagerAdapter(@NonNull FragmentManager fm, Context context, Fragment fragment_default, int default_title) {
        super(fm);
        this.context = context;
        this.items = new ArrayList<>();
        this.titles = new ArrayList<>();
        this.fragment_default = fragment_default;
        this.default_title = default_title;
    }

    public void setItems(ArrayList<Fragment> items) {
        this.items = items;
    }

    public void setTitles(ArrayList<Integer> titles) { this.titles = titles; }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (this.items.size() == 0)
            return this.fragment_default;
        return this.items.get(position);
    }

    @Override
    public int getCount() {
        return (this.items.size() > 0) ? this.items.size() : 1;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (this.titles.size() > 0) return this.context.getString(this.titles.get(position));
        return this.context.getString(this.default_title);
    }
}
