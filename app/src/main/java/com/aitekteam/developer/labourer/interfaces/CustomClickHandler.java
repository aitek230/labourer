package com.aitekteam.developer.labourer.interfaces;

public interface CustomClickHandler {
    void onItemClick(Object item, int position, int action);
}
