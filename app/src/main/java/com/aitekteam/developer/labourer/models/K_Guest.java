package com.aitekteam.developer.labourer.models;

import java.io.Serializable;

public class K_Guest implements Serializable {
    private String
            guest_id;
    private int status;
    private long priceCurrent;

    public K_Guest() {
    }

    public K_Guest(String guest_id, int status, long priceCurrent) {
        this.guest_id = guest_id;
        this.status = status;
        this.priceCurrent = priceCurrent;
    }

    public String getGuest_id() {
        return guest_id;
    }

    public void setGuest_id(String guest_id) {
        this.guest_id = guest_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getPriceCurrent() {
        return priceCurrent;
    }

    public void setPriceCurrent(long priceCurrent) {
        this.priceCurrent = priceCurrent;
    }
}
