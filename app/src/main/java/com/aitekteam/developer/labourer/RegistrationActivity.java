package com.aitekteam.developer.labourer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aitekteam.developer.labourer.models.M_User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistrationActivity extends AppCompatActivity {

    private TextView registration_do_login;
    private EditText registration_full_name, registration_id, registration_email, registration_password;
    private Button registration;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        this.registration_do_login = findViewById(R.id.registration_do_login);
        this.registration_full_name = findViewById(R.id.registration_full_name);
        this.registration_id = findViewById(R.id.registration_id);
        this.registration_email = findViewById(R.id.registration_email);
        this.registration_password = findViewById(R.id.registration_password);
        this.registration = findViewById(R.id.registration);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        // Initialize Firebase Auth
        this.mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = this.mAuth.getCurrentUser();
        if (currentUser != null) goToMain();

        this.registration_do_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        this.registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doRegistration();
            }
        });
    }

    private void doRegistration() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.label_user_type)
            .setItems(R.array.select_user_type, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, final int which) {
                    final String email = registration_email.getText().toString().trim();
                    final String password = registration_password.getText().toString().trim();
                    final String full_name = registration_full_name.getText().toString().trim();
                    final String id = registration_id.getText().toString().trim();

                    if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || TextUtils.isEmpty(full_name) || TextUtils.isEmpty(id)) {
                        Toast.makeText(getApplicationContext(), R.string.msg_registration, Toast.LENGTH_LONG).show();
                        return;
                    }

                    registrationProcess(id, email, full_name, password, which);
                }
            });
        builder.create().show();
    }

    public void registrationProcess(final String id, final String email, final String full_name, final String password, final int which) {
        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        if (mAuth.getUid() != null)
                        mDatabase.child("m_users").child(mAuth.getUid())
                                .setValue(new M_User(
                                        mAuth.getUid(),
                                        id,
                                        email,
                                        full_name,
                                        password,
                                        "",
                                        "",
                                        "",
                                        which
                                )).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(getApplicationContext(), R.string.msg_registration_success, Toast.LENGTH_LONG).show();
                                    goToMain();
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.msg_registration_failed, Toast.LENGTH_LONG).show();
                    }
                }
            });
    }

    private void goToMain() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }
}
